﻿using System.Net;

namespace SignalRChat.Models
{
    public class ApiResultModel : BaseApiResultModel
    {
        public string ErrCode { get; set; }

        public string SuccCode { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
