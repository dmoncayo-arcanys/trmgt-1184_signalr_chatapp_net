﻿using SignalRChat.Entities;
using SignalRChat.Services.Core;

namespace SignalRChat.Services.Interface
{
    public interface IChatService : IActionManager<Chat>
    {
    }
}
