﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SignalRChat.Entities;
using SignalRChat.Repositories.Interface;
using SignalRChat.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SignalRChat.Services.Service
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<UserService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public UserService(IUnitOfWork unitOfWork, IRepository repository, ILogger<UserService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// UnitOfWork.
        /// </summary>
        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Delete record.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var course = repository.All<User>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == id.ToString());
                if (course != null)
                {
                    repository.Delete(course);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Get single record.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Users</returns>
        public User Get(int? id)
        {
            User data = null;
            try
            {
                data = repository.All<User>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == id.ToString());
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return data;
        }

        /// <summary>
        /// Get records.
        /// </summary>
        /// <returns>IEnumerable<Users></returns>
        public IEnumerable<User> GetAll()
        {
            IEnumerable<User> course = null;
            try
            {
                course = repository.All<User>()
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Insert record.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public bool Insert(User entity)
        {
            bool flag = true;
            try
            {
                if (entity != null)
                {
                    repository.Create(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Update record.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public bool Update(User entity)
        {
            bool flag = true;
            try
            {
                var find = repository.All<User>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == entity.Id);
                if (find != null)
                {
                    repository.Update(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
