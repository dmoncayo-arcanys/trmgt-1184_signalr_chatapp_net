﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SignalRChat.Entities;
using SignalRChat.Repositories.Interface;
using SignalRChat.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SignalRChat.Services.Service
{
    public class ChatService : IChatService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<ChatService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public ChatService(IUnitOfWork unitOfWork, IRepository repository, ILogger<ChatService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// UnitOfWork.
        /// </summary>
        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Delete record.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var course = repository.All<Chat>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == id);
                if (course != null)
                {
                    repository.Delete(course);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Get single record.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Users</returns>
        public Chat Get(int? id)
        {
            Chat data = null;
            try
            {
                data = repository.All<Chat>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return data;
        }

        /// <summary>
        /// Get records.
        /// </summary>
        /// <returns>IEnumerable<Users></returns>
        public IEnumerable<Chat> GetAll()
        {
            IEnumerable<Chat> course = null;
            try
            {
                course = repository.All<Chat>()
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Insert record.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public bool Insert(Chat entity)
        {
            bool flag = true;
            try
            {
                if (entity != null)
                {
                    repository.Create(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Update record.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public bool Update(Chat entity)
        {
            bool flag = true;
            try
            {
                var find = repository.All<Chat>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.Id == entity.Id);
                if (find != null)
                {
                    repository.Update(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
