﻿using SignalRChat.Repositories.Interface;
using System.Collections.Generic;

namespace SignalRChat.Services.Core
{
    public interface IActionManager<T> where T : class
    {
        IUnitOfWork UnitOfWork { get; }
        IEnumerable<T> GetAll();
        T Get(int? id);
        bool Insert(T entity);
        bool Update(T entity);
        bool Delete(int? id);
        void SaveChanges();
    }
}
