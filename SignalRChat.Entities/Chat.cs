﻿namespace SignalRChat.Entities
{
    public class Chat
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public long Timestamp { get; set; }
        public string UserId { get; set; }
    }
}
