﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SignalRChat.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        [Column("PhoneNumber")]
        public string Photo { get; set; }
    }
}
