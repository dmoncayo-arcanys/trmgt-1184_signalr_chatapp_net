﻿namespace SignalRChat.Commons
{
    public class Constants
    {
        // URL
        public const string URL_IDENTITY_SERVER = "https://signalrdaleis.azurewebsites.net/";
        public const string URL_API = "https://signalrdaleapi.azurewebsites.net/";
        public const string URL_ANGULAR = "https://signalrdale.azurewebsites.net";

        // Config
        public const string VERSION = "v1";
        public const string CLIENT_ANGULAR = "client_angular";

        // DB
        public const string DB_CONTEXT = "DbContext";
        public const string ASSEMBLY_API = "SignalRChat.API";
    }
}
