﻿using IdentityServer4;
using IdentityServer4.Models;
using SignalRChat.Commons;
using System.Collections.Generic;

namespace SignalRChat.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources() =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResources.Phone(),
            };

        public static IEnumerable<ApiResource> GetApiResources() =>
            new List<ApiResource>
            {
                new ApiResource(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<ApiScope> GetApiScopes() =>
            new List<ApiScope>
            {
                 new ApiScope(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<Client> GetClients() =>
            new List<Client>
            {
               new Client {
                    ClientId = Constants.CLIENT_ANGULAR,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    RedirectUris = { Constants.URL_ANGULAR },
                    PostLogoutRedirectUris = { Constants.URL_ANGULAR },
                    AllowedCorsOrigins = { Constants.URL_ANGULAR },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.Phone,
                        Constants.ASSEMBLY_API,
                    },
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
               }
            };
    }
}
