#pragma checksum "C:\Users\dmoncayo\Documents\Projects\TRAINING\SIGNALR\SignalRChat.API\SignalRChat.IdentityServer\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5ab94408600fe74c91c2c4553c68a98e434f3135"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5ab94408600fe74c91c2c4553c68a98e434f3135", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c66e443ba81bfd444e2b1c1ae94c4deedf2b8d44", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""container"">
    <div class=""row align-items-center justify-content-center min-vh-100 gx-0"">

        <div class=""col-12 col-md-6 col-lg-5"">

            <div class=""card card-shadow border-0"">
                <div class=""card-body position-relative pt-0 mb-6"">
                    <div class=""position-absolute top-0 start-50 translate-middle"">
                        <a class=""avatar avatar-xl mx-auto border border-light border-5 btn-primary""><span class=""mt-7"" style=""margin-left: 7px"">ChatApp</span>
                        </a>
                    </div>
                </div>
                <div class=""card-body"">
                    <div class=""row g-6"">
                        <div class=""col-12"">
                            <div class=""text-center"">
                                <h1 class=""fw-bold"">Welcome to the SignalR Chat App</h1>
                                <p>SignalR Training Project</p>
                            </div>
                        </div>
        ");
            WriteLiteral(@"                <div class=""col-12"">
                            <a href=""https://signalrdale.azurewebsites.net"">
                                <button class=""btn btn-block btn-lg btn-primary w-100 mb-4"" type=""submit"">Continue to Chat App</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=""text-center mt-8"">
                <p>Copyright 2021 . v1.0.1</p>
            </div>

        </div>
    </div>
    <!-- / .row -->
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
