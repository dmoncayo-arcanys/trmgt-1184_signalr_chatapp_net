﻿using Microsoft.AspNetCore.Identity;

namespace SignalRChat.IdentityServer.Models
{
    public class AppUser : IdentityUser
    {
    }
}
