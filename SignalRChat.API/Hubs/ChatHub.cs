﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalRChat.API.Hubs
{
    public class ChatHub : Hub
    {
        public async Task Online(object data)
        {
            await Clients.All.SendAsync("online", data);
        }
    }
}
