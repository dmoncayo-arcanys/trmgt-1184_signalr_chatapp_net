﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.API.Hubs;
using SignalRChat.Entities;
using SignalRChat.Models;
using SignalRChat.Services.Interface;

namespace SignalRChat.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class ChatController : Controller
    {
        private readonly IChatService chatService;

        private readonly IHubContext<ChatHub> chatHub;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="chatService"></param>
        public ChatController(IChatService chatService, IHubContext<ChatHub> chatHub)
        {
            this.chatService = chatService;
            this.chatHub = chatHub;
        }

        /// <summary>
        /// GET: /Users
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetChat()
        {
            ApiResultModel results = new();
            results.Response = chatService.GetAll();
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// GET: /Users
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult SaveChat(Chat data)
        {
            ApiResultModel results = new();
            results.Response = chatService.Insert(data);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            chatHub.Clients.All.SendAsync("send", data);
            return Ok(results);
        }
    }
}
