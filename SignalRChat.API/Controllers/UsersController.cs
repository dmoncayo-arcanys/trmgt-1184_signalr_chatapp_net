﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SignalRChat.Models;
using SignalRChat.Services.Interface;

namespace SignalRChat.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="userService"></param>
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// GET: /Users
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetUsers()
        {
            ApiResultModel results = new();
            results.Response = userService.GetAll();
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }
    }
}
