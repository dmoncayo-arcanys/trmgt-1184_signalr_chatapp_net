﻿using Microsoft.Extensions.DependencyInjection;
using SignalRChat.Repositories.Interface;
using SignalRChat.Repositories.Repository;
using SignalRChat.Services.Interface;
using SignalRChat.Services.Service;

namespace SignalRChat.API
{
    public partial class Startup
    {
        private static void ConfigureDI(IServiceCollection services)
        {
            services.AddScoped<IDbFactory, DbFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IChatService, ChatService>();

            services.AddScoped<IRepository, Repository>();
        }
    }
}
