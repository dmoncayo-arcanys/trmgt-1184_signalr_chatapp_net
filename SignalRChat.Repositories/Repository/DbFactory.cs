﻿using SignalRChat.Repositories.Interface;
using System;

namespace SignalRChat.Repositories.Repository
{
    public class DbFactory : IDbFactory, IDisposable
    {

        private readonly AppDbContext context;

        public DbFactory(AppDbContext context)
        {
            this.context = context;
        }

        public AppDbContext GetDbContext
        {
            get
            {
                return context;
            }
        }

        private bool isDisposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
            isDisposed = true;
        }
    }
}
