﻿namespace SignalRChat.Repositories.Interface
{
    public interface IDbFactory
    {
        AppDbContext GetDbContext { get; }
    }
}
