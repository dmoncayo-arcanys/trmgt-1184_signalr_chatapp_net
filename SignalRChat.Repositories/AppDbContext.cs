﻿using Microsoft.EntityFrameworkCore;
using SignalRChat.Entities;

namespace SignalRChat.Repositories
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .ToTable("AspNetUsers", "Identity")
                .Metadata
                .SetIsTableExcludedFromMigrations(true);
            modelBuilder
                .Entity<Chat>()
                .ToTable("Chat");
        }

        public virtual void Save()
        {
            base.SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}
